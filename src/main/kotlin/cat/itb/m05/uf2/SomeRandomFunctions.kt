package cat.itb.m05.uf2

import kotlin.math.abs

/**
 * @author Oriol Teodoro de la Torre
 */

//input m=7, n=3 output 7 6 5 4 3

/**
 * Retorna tots els números que hi ha entre dos valors
 * @param firstNumber
 * @param secondNumber
 * @return Una llista de tots el números que hi ha entre dos valors
 */
fun numerosEntreDosValors(firstNumber: Int, secondNumber: Int): List<Int> {
    val numeros = mutableListOf<Int>()
    for (numero in firstNumber downTo secondNumber) {
        numeros.add(numero)
    }
    return numeros
}


/**
 * Retorna el valor absolut de la llista
 * @param paraula
 * @return el valor absolut del que es troba a la llista amb map
 * */
fun valorAbsolut(paraula: List<Int>): List<Int> {
    return paraula.map{ abs(it) }
}


// S'esepra el seguent comportament
//  0: No presentat
//  1-4: Insuficient
//  5-6: Suficient
//  7-8: Notable
//  9: Excel·lent
//  10: Excel·lent + MH

/**
 * Retorna l'avaluació d'una nota
 * @param nota Nota en valor enter
 * @return Avaluació en String respecte la nota passada
 */
fun avaluacio(nota: Int): String {
    //TODO: Cahotic order. Make it easy, only one return.
    when (nota) {
        1, 2, 3, 4 -> return "Insuficient"
        0 -> return "No presentat"
        10 -> return "Excelent + MH"
        5, 6 -> return "Suficient"
        7 -> return "Notable"
        else -> return "No valid"
    }
}


/**
 * Retorna les posicions on la paraula conté la lletra passada
 * @param paraula Paraula en String
 * @param lletra Lletra en Char
 * @return La informació que a quedat al contador
 */
fun contarQuantsCopsHiHaUnCaracter(paraula: String, lletra: Char): Int {
    val posicions = mutableListOf<Int>()
    var contador = 0
    for (char in 0 until paraula.lastIndex) {
        if (paraula[char] == lletra) {
            contador++
        }
    }
    return contador
}


//abc="marieta" b='a' -> 6
//abc="marieta" b='b' -> -1

/**
 * Si existeix la lletra dins de la paraula, indica en quina posicio esta la lletra. Pero si no existeix ho indica amb un -1.
 * @param paraula Paraula en String
 * @param lletra Lletra en Char
 * @return La posicio de la lletra a dins de la paraula si existeix
 * @return El valor -1 si a dins de la paraula no existeix la lletra
 **/
fun lletraAdinsDeLaParaula(paraula: String, lletra: Char): Int {
    val posicions: MutableList<Int> = mutableListOf()
    for (char in paraula.lastIndex downTo 0) {
        if (paraula[char] == lletra) {
            return char
        }
    }
    return -1
}


//La funció ha de retornar la nota arrodonida. 8.7 -> 9, 7.5 -> 8
//Excepció, Si la nota no arriba a un 5, no obtindrà el 5. És a dir: 4.9 -> 4

/**
 * Retorna la nota que li passem, però arrodonida, excepte el 5. Que l'arrodoneix al 4
 * @param nota Nota amb decimals
 * @return Nota arrodonida com a valor enter
 */
fun notaArrodonida(nota: Double): Int {
    return nota.toInt()
}