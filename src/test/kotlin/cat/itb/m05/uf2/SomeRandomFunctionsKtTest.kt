package cat.itb.m05.uf2

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class SomeRandomFunctionsKtTest {

    @Test
    fun mateixNumeroEntreDosValorsTest() {
        val firstNumber = 3
        val secondNumber = 7
        val expected = listOf(3, 4, 5, 6, 7)
        assertEquals(expected, numerosEntreDosValors(firstNumber, secondNumber))
    }

    @Test
    fun mateixaQuantitatDeNumerosEntreDosValorsTest() {
        val firstNumber = 3
        val secondNumber = 7
        val expected = 5
        assertEquals(expected, numerosEntreDosValors(firstNumber, secondNumber).size)
    }

    @Test
    fun conteTotsUnNumeroIndicatEntreDosValorsTest() {
        val firstNumber = 3
        val secondNumber = 7
        assertTrue(numerosEntreDosValors(firstNumber, secondNumber).contains(4))
    }

    @Test
    fun valorAbsolut() {
    }

    @Test
    fun avaluacioCorrecteSegonsLaNotaTest() {
        val notaAlumne0 = 0
        val notaAlumne1 = 1
        val notaAlumne2 = 2
        val notaAlumne3 = 3
        val notaAlumne4 = 4
        val notaAlumne5 = 5
        val notaAlumne6 = 6
        val notaAlumne7 = 7
        val notaAlumne8 = 9
        val notaAlumne9 = 10
        assertAll(
            { assertEquals("No presentat", avaluacio(notaAlumne0)) },
            { assertEquals("Insuficient", avaluacio(notaAlumne1)) },
            { assertEquals("Insuficient", avaluacio(notaAlumne2)) },
            { assertEquals("Insuficient", avaluacio(notaAlumne3)) },
            { assertEquals("Insuficient", avaluacio(notaAlumne4)) },
            { assertEquals("Suficient", avaluacio(notaAlumne5)) },
            { assertEquals("Suficient", avaluacio(notaAlumne6)) },
            { assertEquals("Notable", avaluacio(notaAlumne7)) },
            { assertEquals("No valid", avaluacio(notaAlumne8)) },
            { assertEquals("Excelent + MH", avaluacio(notaAlumne9)) }
        )
    }

    @Test
    fun contarQuantsCopsHiHaUnCaracterTest() {
    }


    @Test
    fun mateixaPosicioDeLaPrimeraCoincidenciaAmbAquestaEntremitgTest() {
        val paraula = "caracola"
        val lletra = 'a'
        val expected = 1
        assertEquals(expected, lletraAdinsDeLaParaula(paraula, lletra))
    }

    @Test
    fun mateixaPosicioDeLaPrimeraCoincidenciaAmbAquestaAlPrincipiTest() {
        val paraula = "amic"
        val lletra = 'a'
        val expected = 0
        assertEquals(expected, lletraAdinsDeLaParaula(paraula, lletra))
    }

    @Test
    fun mateixaPosicioDeLaPrimeraCoincidenciaAmbAquestaLletraAlFinalTest() {
        val paraula = "musica"
        val lletra = 'a'
        val expected = 5
        assertEquals(expected, lletraAdinsDeLaParaula(paraula, lletra))
    }

    @Test
    fun posicioNoValidaAunaParaulaTest() {
        val paraula = "musica"
        val lletra = 'b'
        val expected = -1
        assertEquals(expected, lletraAdinsDeLaParaula(paraula, lletra))
    }

    @Test
    fun `nota ben arrodonida de 4,9 a 4 Test`() {
        val expected = 4
        val nota = 4.9
        assertEquals(expected, notaArrodonida(nota))
    }

    @Test
    fun `nota ben arrodonida de (valor,valor més gran que 5) a número més Test`() {
        val expected = 7
        val nota = 6.7
        assertEquals(expected, notaArrodonida(nota))
    }

    @Test
    fun `nota ben arrodonida de ,5 a un número més Test`() {
        val expected = 8
        val nota = 7.5
        assertEquals(expected, notaArrodonida(nota))
    }
}